package qa.plates;

public class BadNumException extends Exception {
    private String message;

    public BadNumException(String errorMessage) {
        message = errorMessage;
    }

    public String getMessage(){
        return message;
    }
}
