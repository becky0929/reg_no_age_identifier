package qa.plates;

public class NumberCombos {

    public String generateNewNum(String input) throws BadNumException {
        if(input.equals("99") || input.equals("50") || input.equals("01")){
            throw new BadNumException("you cannot use these years ");
        }
        int convertedNum = Integer.parseInt(input);
        String convertedNumString = null;

        if ( convertedNum < 51) {
            convertedNum += 50;
            convertedNumString = Integer.toString(convertedNum);
            System.out.println(convertedNumString);
        }
        else if( convertedNum >=51 && convertedNum < 99){
            convertedNum -= 49;
            convertedNumString = Integer.toString(convertedNum);
            if(convertedNum <10 ){
                System.out.println("0"+convertedNumString);
                return ("0"+convertedNumString);
            }
            else{
                System.out.println(convertedNumString);
            } 
        }
        return convertedNumString;
    }
}
