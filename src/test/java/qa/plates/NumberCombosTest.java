package qa.plates;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


public class NumberCombosTest {

    @Test
    public void pass_in_a_march_number_september_is_returned() throws BadNumException {
        String input = "05";
        String expectedResult = "55";
        NumberCombos cut = new NumberCombos();

        String convertedActualResult = cut.generateNewNum(input);

        assertEquals(expectedResult, convertedActualResult);
    }

    @Test
    public void pass_in_a_september_77_number_march_is_returned_year_after_28() throws BadNumException {
        String input = "77";
        String expectedResult = "28";
        NumberCombos cut = new NumberCombos();

        String convertedActualResult = cut.generateNewNum(input);

        assertEquals(expectedResult, convertedActualResult);
    }

    @Test
    public void pass_in_a_52_number_march_is_returned_year_after_03() throws BadNumException {
        String input = "52";
        String expectedResult = "03";
        NumberCombos cut = new NumberCombos();

        String convertedActualResult = cut.generateNewNum(input);

        assertEquals(expectedResult, convertedActualResult);
    }

    @Test
    public void pass_in_a_51() throws BadNumException {
        String input = "51";
        String expectedResult = "02";
        NumberCombos cut = new NumberCombos();

        String convertedActualResult = cut.generateNewNum(input);

        assertEquals(expectedResult, convertedActualResult);
    }


    @Test
    public void test_99() throws BadNumException {
        String input = "99";
        NumberCombos cut = new NumberCombos();

        //act
        assertThrows(BadNumException.class,
                () -> cut.generateNewNum(input));
    }

    @Test
    public void test_50() throws BadNumException {
        String input = "50";
        NumberCombos cut = new NumberCombos();

        //act
        assertThrows(BadNumException.class,
                () -> cut.generateNewNum(input));
    }

    @Test
    public void test_01() throws BadNumException {
        String input = "01";
        NumberCombos cut = new NumberCombos();

        //act
        assertThrows(BadNumException.class,
                () -> cut.generateNewNum(input));
    }

    @Test
    public void over_99() throws BadNumException {
        String input = "100";
        String expectedResult = null;
        NumberCombos cut = new NumberCombos();

        String convertedActualResult = cut.generateNewNum(input);

        assertEquals(expectedResult, convertedActualResult);
    }

}
